From iris.algebra Require Import auth agree.
From iris.base_logic Require Import invariants.
From iris.heap_lang Require Export lifting metatheory.
From iris.heap_lang Require Import notation proofmode.

From iris_examples.logrel_heaplang Require Import ltyping ltyping_safety.


(** Semantic typing "branded types" for unchecked indexing into a fixed-size
array. *)

(* utility stuff *)
Ltac bool_decide H :=
  match goal with
  | [ |- context[@bool_decide ?P ?d]] =>
    destruct d as [H|H];
    [rewrite  bool_decide_true; [|exact H]|rewrite  bool_decide_false; [|exact H]]
  end.

Lemma Z_to_nat (n: Z): 0 ≤ n -> ∃ m: nat, n = m.
Proof.
  intros; exists (Z.to_nat n); rewrite Z2Nat.id; lia.
Qed.

(** Arrays *)
Section Arrays.

  Context `{! heapG Σ}.
  (* Arrays have the following memory layout: |n|x_0|...|x_{n-1}| *)

  (* to create an array, unsafe *)
  Definition unsafe_arr : val :=
    λ: "n", let: "a" := AllocN ("n" + #1) #0 in "a" <- "n" ;; "a".

  Definition unsafe_set : val :=
    λ: "a" "i" "x", ("a" + "i" + #1) <- "x".

  Definition unsafe_get : val :=
    λ: "a" "i", !("a" + "i" + #1).

  Definition arr : val :=
    λ: "n", if: "n" < #0 then NONE else SOME (unsafe_arr "n").

  Definition get : val :=
    λ: "a" "i", if: ((#-1) < "i") && ("i" < !"a") then SOME (unsafe_get "a" "i") else NONE.

  Definition set : val :=
    λ: "a" "i" "x", if: ((#-1) < "i") && ("i" < !"a") then (unsafe_set "a" "i" "x") else #().

  (* Verification *)
  Variable (N: namespace).
  Definition array (n: Z) (l: loc) : iProp Σ :=
    (∃ zs: list Z, ⌜n = length zs⌝ ∗ l ↦∗ (#n :: ((LitV ∘ LitInt) <$> zs)))%I.
  Definition body (n: Z) (l: loc) : iProp Σ :=
    (∃ zs: list Z, ⌜n = length zs⌝ ∗ (l +ₗ 1) ↦∗ ((LitV ∘ LitInt) <$> zs))%I.
  Definition entry (l: loc) (i: Z) : iProp Σ :=
    (∃ z: Z, (l +ₗ i +ₗ1) ↦ #z)%I.

  Lemma body_decompose n l:
    ⌜n > 0⌝ -∗ body n l -∗ entry l 0 ∗ body (n - 1) (loc_add l 1).
  Proof.
    iIntros "%". iDestruct 1 as (vs) "[% H]". subst n. destruct vs as [|v vs]; simpl in *; try lia.
    cbn; iDestruct "H" as "[He H]". iSplitL "He".
    { iExists _; replace (0%nat : Z) with 0 by lia; by rewrite !loc_add_0. }
    iExists vs; simpl. iSplitR; first (iPureIntro; lia).
    unfold array.array. iApply big_sepL_mono'; eauto.
    intros n v'; rewrite !loc_add_assoc. simpl.
      by replace (1 + S n) with (1 + (1 + n)) by lia.
  Qed.

  Lemma body_compose n l:
    entry l 0 -∗ body (n - 1) (loc_add l 1)  -∗ body n l.
  Proof.
    iDestruct 1 as (v) "E"; rewrite !loc_add_0.
    iDestruct 1 as (vs) "[% B]". iExists (v::vs). iSplit.
    { iPureIntro; simpl; lia. }
    rewrite array_cons; iFrame.
  Qed.

  Lemma body_entry_acc i n l:
    ⌜0 ≤ i < n⌝ -∗ body n l -∗ entry l i ∗ (entry l i -∗ body n l).
  Proof.
    iIntros ([H1 H2]) "B". edestruct (Z_to_nat i) as [j ->]; first lia.
    edestruct (Z_to_nat n) as [m ->]; first lia. clear H1.
    iInduction m as [|m] "IH" forall (l j H2); first lia. destruct j as [|j].
   all: iDestruct (body_decompose with "[] B") as "[E B]"; first (iPureIntro; lia).
    - replace (0%nat : Z) with 0 by lia.
      iFrame. iIntros "E". iApply (body_compose with "E B").
    - replace (S m - 1) with (m: Z) by lia.
      iDestruct ("IH" $! _ j with "[] B") as "[E2 Bclose]"; first (iPureIntro; lia).
      iSplitL "E2".
      { unfold entry. rewrite !loc_add_assoc. by replace (S j + 1) with (1 + (j + 1)) by lia. }
      iIntros "E2". iApply (body_compose with "E"). replace (S m - 1) with (m: Z) by lia.
      iApply "Bclose".
      { unfold entry. rewrite !loc_add_assoc. by replace (S j + 1) with (1 + (j + 1)) by lia. }
  Qed.

  Lemma array_body_acc n l:
    array n l -∗ body n l ∗ (body n l -∗ array n l).
  Proof.
    iDestruct 1 as (vs) "[% A]"; rewrite array_cons. iDestruct "A" as "[L B]".
    iSplitL "B"; first (iExists vs ; by iSplit).
    iDestruct 1 as (vs') "[% B]". iExists vs'. rewrite array_cons. iFrame. by iPureIntro.
  Qed.

  Lemma array_entry_acc i n l:
    ⌜0 ≤ i < n⌝ -∗ array n l -∗ entry l i ∗ (entry l i -∗ array n l).
  Proof.
    iIntros "H A". iDestruct (array_body_acc with "A") as "[B Bclose]".
    iDestruct (body_entry_acc with "H B") as "[E Eclose]". iFrame.
    iIntros "E". iApply "Bclose". by iApply "Eclose".
  Qed.

  Lemma array_length_acc n l:
    array n l -∗ l ↦ #n ∗ (l ↦ #n -∗ array n l).
  Proof.
    iDestruct 1 as (zs) "[H1 H2]"; cbn; iDestruct "H2" as "[Hl H2]".
    rewrite loc_add_0. iFrame. iIntros "Hl". iExists _. iFrame "H1".
    cbn; iFrame. by rewrite loc_add_0.
  Qed.

  Lemma body_entry_inv i n l:
    ⌜0 ≤ i < n⌝ -∗ inv N (body n l) -∗ inv N (entry l i).
  Proof.
    iIntros "% #I". iApply inv_acc; eauto. iNext. iModIntro. by iApply body_entry_acc.
  Qed.

  Lemma array_entry_inv i n l:
    ⌜0 ≤ i < n⌝ -∗ inv N (array n l) -∗ inv N (entry l i).
  Proof.
    iIntros "% #I". iApply inv_acc; eauto. iNext. iModIntro. by iApply array_entry_acc.
  Qed.

  Lemma array_body_inv n l:
    inv N (array n l) -∗ inv N (body n l).
  Proof.
    iIntros "#I". iApply inv_acc; eauto. iNext. iModIntro. by iApply array_body_acc.
  Qed.

  Lemma array_length_inv n l:
    inv N (array n l) -∗ inv N (l ↦ #n).
  Proof.
    iIntros "#I". iApply inv_acc; eauto. iNext. iModIntro. by iApply array_length_acc.
  Qed.

  (** ** Specs *)
  Lemma unsafe_arr_spec n: {{{ ⌜0 ≤ n⌝ }}} unsafe_arr #n {{{ (l: loc), RET #l; array n l }}}.
  Proof.
    iIntros (ϕ) "% Post". wp_lam. wp_pures. wp_bind (AllocN _ _).
    wp_apply (wp_allocN); eauto. lia.
    iIntros (l) "[A _]". wp_pures. replace (n + 1) with (Z.succ n) by lia.
    rewrite Z2Nat.inj_succ; try lia.
    iPoseProof (@update_array _ _ _ _ 0%nat with "[$A]") as "[Hl B]"; first reflexivity.
    rewrite loc_add_0. wp_store. iApply "Post". iSpecialize ("B" with "[$Hl]"). simpl.
    iExists (replicate (Z.to_nat n) 0). iSplit; eauto.
    { iPureIntro. rewrite replicate_length. symmetry. apply Z2Nat.id. lia. }
      by rewrite fmap_replicate.
  Qed.

  Lemma unsafe_get_spec n a i:
    {{{ inv N (array n a) ∗ ⌜0 ≤ i < n⌝ }}} unsafe_get #a #i {{{ (x: Z), RET #x; True }}}.
  Proof.
    iIntros (ϕ) "[#A %] Post". unfold unsafe_get. wp_pures.
    iPoseProof (array_entry_inv with "[] A") as "E"; eauto.
    iInv "E" as ">HE" "Hclose". iDestruct "HE" as (v) "He".
    wp_load. iMod ("Hclose" with "[He]") as "_"; first by iExists _.
    now iApply "Post".
  Qed.

  Lemma unsafe_set_spec n a i (x: Z):
    {{{ inv N (array n a) ∗ ⌜0 ≤ i < n⌝ }}} unsafe_set #a #i #x {{{ RET #(); True }}}.
  Proof.
    iIntros (ϕ) "[#A %] Post". unfold unsafe_set. wp_pures.
    iPoseProof (array_entry_inv with "[] A") as "E"; eauto.
    iInv "E" as ">HE" "Hclose". iDestruct "HE" as (v) "He".
    wp_store. iMod ("Hclose" with "[He]") as "_"; first by iExists _.
    now iApply "Post".
  Qed.

  (** Semantic Types *)
  Definition Option (S: lty Σ) : lty Σ :=
    Lty (λ v, (□ ⌜v = NONEV⌝ ∨ ∃ s, S s ∗ ⌜v = SOMEV s⌝)%I).
  Definition Array : lty Σ :=
    Lty (λ v, (□ ∃ (a: loc) (m: mnat), inv N (array m a) ∗ ⌜v = #a⌝)%I).

  Lemma typing_arr: ∅ ⊨ arr : lty_int → Option Array.
  Proof.
    iIntros (s) "!# _ /=". iApply wp_value.
    iIntros (v1) "!#". iDestruct 1 as (z) "->".
    unfold arr. wp_pures.
    bool_decide H; wp_pures.
    - iLeft. eauto.
    - wp_apply wp_fupd.
      wp_apply (unsafe_arr_spec with "[]"); eauto with lia.
      iIntros (l) "A".
      edestruct (Z_to_nat z) as [m ->]; try lia.
      wp_pures.
      iMod (inv_alloc N _ (array m l) with "[$A]") as "#I".
      iModIntro. iRight.
      iExists #l. iSplit; eauto.
      iExists l, m. iModIntro. by iSplit.
  Qed.

  Lemma typing_get: ∅ ⊨ get : Array → lty_int → Option lty_int.
  Proof.
    iIntros (s) "!# _ /=". iApply wp_value.
    iIntros (v) "!#". iDestruct 1 as (a m) "[#I ->]".
    unfold get. wp_pures.
    iIntros (v) "!#". iDestruct 1 as (i) "->".
    wp_pures.
    bool_decide H; wp_pures.
    - wp_bind (! _)%E. iPoseProof (array_length_inv with "I") as "L".
      iInv "L" as "Hl" "Hclose". wp_load.
      iMod("Hclose" with "Hl") as "_". iModIntro. wp_pures. bool_decide H'.
      + wp_pures. wp_apply (unsafe_get_spec); first iSplit; eauto with lia.
        iIntros (x) "_". wp_pures. iRight. iExists #x. iSplit; eauto.
      + wp_pures.  now iLeft.
    - wp_pures. now iLeft.
  Qed.

  Lemma typing_set: ∅ ⊨ set : Array → lty_int → lty_int → lty_unit.
  Proof.
    iIntros (s) "!# _ /=". iApply wp_value.
    iIntros (v) "!#". iDestruct 1 as (a m) "[#I ->]".
    unfold set. wp_pures.
    iIntros (v) "!#". iDestruct 1 as (i) "->".
    wp_pures.
    iIntros (v) "!#". iDestruct 1 as (x) "->".
    wp_pures.
    bool_decide H; wp_pures.
    - wp_bind (! _)%E. iPoseProof (array_length_inv with "I") as "L".
      iInv "L" as "Hl" "Hclose". wp_load.
      iMod("Hclose" with "Hl") as "_". iModIntro. wp_pures. bool_decide H'.
      + wp_pures. wp_apply (unsafe_set_spec); first iSplit; eauto with lia.
      + now wp_pures.
    - now wp_pures.
  Qed.

  (** Copying Arrays *)
  Definition copy_rec : val :=
    rec: "f" "old" "new" "n" :=
      if: #0 < "n" then ("new" + #1) <- !("old" + #1);; "f" ("old" + #1) ("new" + #1) ("n" - #1)
      else #().

  Definition copy : val :=
    λ: "a" "n", let: "b" := unsafe_arr "n" in copy_rec "a" "b" (!"a");; "b".

  Lemma copy_rec_spec (a1 a2: loc) (n1 n2 n: Z):
    {{{ inv N (body n1 a1) ∗ body n2 a2 ∗ ⌜n ≤ n1⌝ ∗ ⌜n ≤ n2⌝ }}}
      copy_rec #a1 #a2 #n
    {{{ RET #(); body n2 a2 }}}.
  Proof.
    iIntros (ϕ) "(#I & B & % & %) Post".
    iLöb as "IH" forall (a1 a2 n n1 n2 H H0) "I".
    unfold copy_rec. wp_pures. fold copy_rec.
    bool_decide H1.
    - wp_pures. iPoseProof (body_entry_inv 0 with "[] I") as "E"; first (iPureIntro; split; lia).
      wp_bind(! _)%E. iInv "E" as (v) "E" "Hclose". rewrite loc_add_0.
      wp_load. iMod ("Hclose" with "[E]") as "_"; first (iExists _; by rewrite loc_add_0).
      iModIntro. wp_pures. iPoseProof (body_entry_acc 0 with "[] B") as "[E Eclose]"; first (iPureIntro; split; lia).
      wp_bind(_ <- _)%E. iDestruct "E" as (v') "E". rewrite loc_add_0.
      wp_store. wp_pures. iDestruct ("Eclose" with "[E]") as "B"; first (iExists _; by rewrite loc_add_0).
      iDestruct (body_decompose with "[] B") as "[E B]"; first (iPureIntro; lia).
      wp_apply ("IH" $! _ _ _ (n1 - 1) (n2 - 1) with "[] [] B [E Post]").
      1 - 2: iPureIntro; lia.
      + iNext. iIntros "B". iApply "Post". iApply (body_compose with "E B").
      + iModIntro. iApply inv_acc; eauto. iNext. iModIntro.
        iIntros "B". iPoseProof (body_decompose with "[] B") as "[E B]"; first (iPureIntro; lia).
        iFrame. iIntros "B". iApply (body_compose with "E B").
    - wp_pures; iApply "Post"; eauto.
  Qed.

  Lemma copy_spec (a: loc) (m n: Z):
    {{{ inv N (array m a) ∗ ⌜0 ≤ m ≤ n⌝}}}
      copy #a #n
    {{{ (b: loc), RET #b; array n b }}}.
  Proof.
    iIntros (ϕ) "[#H %] Post".
    unfold copy. wp_pures. wp_apply (unsafe_arr_spec); first (iPureIntro; lia).
    iIntros (l) "A". wp_pures. wp_bind (! _)%E.
    iPoseProof (array_length_inv with "H") as "I".
    iInv "I" as "Ha" "Hclose". wp_load. iMod("Hclose" with "Ha") as "_". iModIntro.
    iPoseProof (array_body_acc with "A") as "[B Bclose]".
    wp_apply (copy_rec_spec with "[$B]").
    { repeat iSplit; eauto with lia. iApply array_body_inv; eauto. }
    iIntros "?". wp_pures. iApply "Post". by iApply "Bclose".
  Qed.
End Arrays.
